package rs.ac.bg.etf.controller;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import rs.ac.bg.etf.database.Database;
import rs.ac.bg.etf.database.Item;
import rs.ac.bg.etf.database.Unit;

public class AddItemController {
	@FXML
	ComboBox<String> unitChoice;
	@FXML
	Button discard;
	@FXML
	Button confirm;
	@FXML
	TextField barcodeInput;
	@FXML
	TextField nameInput;
	@FXML
	TextField sizeInput;
	@FXML
	TextField quantityInput;

	private boolean editMode;

	TableView<Item> table;

	public void initialize(TableView<Item> table) {
		this.table = table;
		unitChoice.setItems(FXCollections.observableArrayList(
				Stream.of(Unit.values()).map(unit -> unit.toString()).collect(Collectors.toList())));
		unitChoice.getSelectionModel().select(2);
		if (editMode) {
			Item item = table.getSelectionModel().getSelectedItem();
			unitChoice.getSelectionModel().select(item.getUnit().ordinal());
			barcodeInput.setText(item.getBarcode());
			barcodeInput.setEditable(false);
			nameInput.setText(item.getName());
			sizeInput.setText("" + item.getSize());
			quantityInput.setText("" + item.getQuantity());
		}
	}

	@FXML
	public void closeForm() {
		((Stage) discard.getScene().getWindow()).close();
	}

	@FXML
	public void submitForm() {
		if (editMode) {
			editItemSubmit();
		} else {
			addItemSubmit();
		}
	}

	private void addItemSubmit() {
		if (!barcodeInput.getText().isEmpty() && !nameInput.getText().isEmpty() && !sizeInput.getText().isEmpty()
				&& !quantityInput.getText().isEmpty()) {
			Item item = new Item(nameInput.getText(), Unit.fromString(unitChoice.getSelectionModel().getSelectedItem()),
					Double.parseDouble(sizeInput.getText().trim()), barcodeInput.getText(),
					Integer.parseInt(quantityInput.getText().trim()));
			Database.getInstance().addItem(item);
			closeForm();
			table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		}
	}

	private void editItemSubmit() {
		if (!barcodeInput.getText().isEmpty() && !nameInput.getText().isEmpty() && !sizeInput.getText().isEmpty()
				&& !quantityInput.getText().isEmpty()) {
			String id = table.getSelectionModel().getSelectedItem().getBarcode();
			Item item = new Item(nameInput.getText(), Unit.fromString(unitChoice.getSelectionModel().getSelectedItem()),
					Double.parseDouble(sizeInput.getText().trim()), barcodeInput.getText(),
					Integer.parseInt(quantityInput.getText().trim()));
			Database.getInstance().updateItem(id, item);
			closeForm();
			table.getItems().clear();
			table.getItems().addAll(Database.getInstance().getItems());
		}
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
}
