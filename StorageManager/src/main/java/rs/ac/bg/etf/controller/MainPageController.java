package rs.ac.bg.etf.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rs.ac.bg.etf.database.Database;
import rs.ac.bg.etf.database.Item;

public class MainPageController {

	@FXML
	private TableView<Item> table;
	@FXML
	private TableColumn<Item, String> barcodeCol;
	@FXML
	private TableColumn<Item, String> nameCol;
	@FXML
	private TableColumn<Item, Number> sizeCol;
	@FXML
	private TableColumn<Item, String> unitCol;
	@FXML
	private TableColumn<Item, Number> quantityCol;

	@FXML
	protected void closeApplication() {
		Platform.exit();
	}

	public void initializeTable() {
		barcodeCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getBarcode()));
		nameCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
		sizeCol.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getSize()));
		unitCol.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUnit().toString()));
		quantityCol.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getQuantity()));
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}

	public void addToTable(Item item) {
		Database.getInstance().addItem(item);
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
	}

	@FXML
	public void openAddForm() {
		Parent mainNode = null;
		AddItemController con = null;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/AddItem.fxml"));
			mainNode = loader.load();
			con = loader.getController();
			con.initialize(table);

		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(mainNode);
		Stage stage = new Stage();
		stage.setScene(scene);

		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}

	@FXML
	public void openEditForm() {
		if (table.getSelectionModel().getSelectedItems().size() != 1) {
			new Alert(AlertType.INFORMATION, "Please select exactly one item.", ButtonType.OK).showAndWait();
			return;
		}
		Parent mainNode = null;
		AddItemController con = null;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/AddItem.fxml"));
			mainNode = loader.load();
			con = loader.getController();
			con.setEditMode(true);
			con.initialize(table);

		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(mainNode);
		Stage stage = new Stage();
		stage.setScene(scene);

		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}

	@FXML
	public void removeSelectedItems() {
		List<Item> selectedItems = table.getSelectionModel().getSelectedItems();
		if (selectedItems.isEmpty()) {
			return;
		}

		for (Item item : selectedItems) {
			System.out.println(item);
			Database.getInstance().removeItem(item);
		}

		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
	}

	@FXML
	public void exportData() {
		DirectoryChooser chooser = new DirectoryChooser();
		File destDir = chooser.showDialog(table.getScene().getWindow());
		if (destDir != null) {
			Util.exportData(destDir.getAbsolutePath() + File.separator + "report.txt");
		}
	}
}
