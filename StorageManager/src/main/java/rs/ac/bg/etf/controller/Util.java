package rs.ac.bg.etf.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import rs.ac.bg.etf.database.Database;
import rs.ac.bg.etf.database.Item;

public class Util {
	public static void exportData(String filePath) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filePath)))) {
			bw.write("Current items present in storage:\n\n\n");

			for (Item item : Database.getInstance().getItems()) {
				bw.write(item.toString());
				bw.write("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
