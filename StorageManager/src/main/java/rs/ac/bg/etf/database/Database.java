package rs.ac.bg.etf.database;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public class Database {

	private static Database instance;
	protected MongoCollection<Document> collection;

	private Database() {
		collection = MongoClients.create().getDatabase("storagemanager").getCollection("items");
	}

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}

		return instance;
	}

	public List<Item> getItems() {
		List<Item> items = new LinkedList<>();

		collection.find().map(d -> new Item(d)).forEach(new Consumer<Item>() {
			public void accept(Item i) {
				items.add(i);
			}
		});

		return items;
	}

	public void removeItem(Item item) {
		collection.deleteOne(Filters.eq("_id", item.getBarcode()));
	}

	public void addItem(Item item) {
		if (collection.countDocuments(new Document().append("_id", item.getBarcode())) == 0) {
			collection.insertOne(item.getAsDocument().append("_id", item.getBarcode()));
		}
	}

	public void updateItem(String id, Item item) {
		collection.replaceOne(new Document().append("_id", id), item.getAsDocument());
	}
}
