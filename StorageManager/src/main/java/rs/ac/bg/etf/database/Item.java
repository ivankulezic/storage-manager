package rs.ac.bg.etf.database;

import org.bson.Document;

public class Item {
	private String name;
	private Unit unit;
	private Double size;
	private String barcode;
	private Integer quantity;

	public Item(String name, Unit unit, double size, String barcode, int quantity) {
		super();
		this.name = name;
		this.unit = unit;
		this.size = size;
		this.barcode = barcode;
		this.quantity = quantity;
	}

	public Item(Document d) {
		this.name = d.getString("name");
		this.unit = Unit.values()[d.getInteger("unit")];
		this.size = d.getDouble("size");
		this.barcode = d.getString("barcode");
		this.quantity = d.getInteger("quantity");
	}

	public String getName() {
		return name;
	}

	public Unit getUnit() {
		return unit;
	}

	public Double getSize() {
		return size;
	}

	public String getBarcode() {
		return barcode;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Document getAsDocument() {
		return new Document("name", name).append("unit", unit.ordinal()).append("quantity", quantity)
				.append("size", size).append("barcode", barcode);
	}

	@Override
	public boolean equals(Object o) {
		return barcode.equals(((Item) o).barcode);
	}

	@Override
	public String toString() {
		return barcode + " " + name + " " + quantity + "x" + size + " " + unit;
	}

}
