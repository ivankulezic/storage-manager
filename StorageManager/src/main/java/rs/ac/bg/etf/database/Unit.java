package rs.ac.bg.etf.database;

public enum Unit {
	KILOGRAM("kg"), MILILITER("ml"), PIECE("pc");

	private String unitCode;

	Unit(String unitCode) {
		this.unitCode = unitCode;
	}

	public String toString() {
		return unitCode;
	}

	public static Unit fromString(String unit) {
		switch (unit.toLowerCase()) {
		case "kg":
			return KILOGRAM;
		case "ml":
			return Unit.MILILITER;
		case "pc":
			return PIECE;
		default:
			return null;
		}
	}
}
