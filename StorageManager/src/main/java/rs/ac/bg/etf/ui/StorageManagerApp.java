package rs.ac.bg.etf.ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import rs.ac.bg.etf.controller.MainPageController;

public class StorageManagerApp extends Application {

	@Override
	public void start(Stage primaryStage) {
		Parent mainNode = null;
		MainPageController controller = null;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/MainPage.fxml"));
			mainNode = loader.load();
			controller = loader.getController();
			controller.initializeTable();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(mainNode, 650, 600);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
