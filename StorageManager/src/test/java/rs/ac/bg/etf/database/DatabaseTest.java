package rs.ac.bg.etf.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.client.model.Filters;

public class DatabaseTest {

	private Item newItem;
	private Item existingItem;

	@Before
	public void setUp() {
		newItem = new Item("Monitor", Unit.PIECE, 1.0, "978020137962", 1);
		existingItem = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().collection
				.insertOne(existingItem.getAsDocument().append("_id", existingItem.getBarcode()));
	}

	@After
	public void tearDown() {
		Database.getInstance().collection.deleteMany(Filters.eq("barcode", newItem.getBarcode()));
		Database.getInstance().collection
				.deleteMany(new Document().append("_id", existingItem.getBarcode()));
	}

	@Test
	public void databaseInitializesProperly() {
		assertNotEquals(0, Database.getInstance().collection.countDocuments());
	}

	@Test
	public void databaseHasExactlyOneEntry() {
		assertEquals(1, Database.getInstance().getItems().size(), 1);
	}

	@Test
	public void addItemToDatabase() {
		Database.getInstance().addItem(newItem);
		assertEquals(2, Database.getInstance().getItems().size());
	}

	@Test
	public void addSameItemTwice() {
		Database.getInstance().addItem(newItem);
		Database.getInstance().addItem(newItem);
		assertEquals(2, Database.getInstance().getItems().size());
	}

	@Test
	public void removeItemFromDatabase() {
		Database.getInstance().removeItem(existingItem);
		assertEquals(0, Database.getInstance().getItems().size());
	}
	
	@Test
	public void updateItemInDatabase() {
		Database.getInstance().updateItem(existingItem.getBarcode(), new Item("Apple", Unit.KILOGRAM, 100.0, "978020137961", 5));
		assertEquals(1, Database.getInstance().getItems().size());
		Item newItem = Database.getInstance().getItems().get(0);
		assertNotEquals(newItem.getName(), existingItem.getName());
		assertNotEquals(newItem.getQuantity(), existingItem.getQuantity());
		assertNotEquals(newItem.getSize(), existingItem.getSize());
		assertNotEquals(newItem.getUnit(), existingItem.getUnit());
	}
}
