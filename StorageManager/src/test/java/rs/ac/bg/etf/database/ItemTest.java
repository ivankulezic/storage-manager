package rs.ac.bg.etf.database;

import static org.junit.Assert.assertEquals;

import org.bson.Document;
import org.junit.Test;

public class ItemTest {
	@Test
	public void itemParsesDocumentProperly() {
		Document doc = new Document("name", "Monitor").append("unit", Unit.PIECE.ordinal()).append("quantity", 1)
				.append("size", 1.0).append("barcode", "978020137962");
		Item item = new Item(doc);
		assertEquals(item.getBarcode(), "978020137962");
		assertEquals(item.getName(), "Monitor");
		assertEquals(item.getQuantity(), new Integer(1));
		assertEquals(item.getSize(), new Double(1.0));
		assertEquals(item.getUnit(), Unit.PIECE);
	}

	@Test
	public void itemGeneratesDocumentProperly() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137962", 1);
		Document doc = item.getAsDocument();

		assertEquals(doc.getString("barcode"), "978020137962");
		assertEquals(doc.getString("name"), "Monitor");
		assertEquals(doc.getInteger("quantity"), new Integer(1));
		assertEquals(doc.getDouble("size"), new Double(1.0));
		assertEquals(Unit.values()[doc.getInteger("unit")], Unit.PIECE);
	}
}
