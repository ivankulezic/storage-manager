package rs.ac.bg.etf.ui;

import org.junit.After;
import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;
import org.testfx.matcher.control.LabeledMatchers;
import org.testfx.service.query.EmptyNodeQueryException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

public class AddItemPageTest extends ApplicationTest {

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(StorageManagerApp.class.getResource("/AddItem.fxml"));
		Parent mainNode = loader.load();
		stage.setScene(new Scene(mainNode));
		stage.show();
		stage.toFront();
	}

	@After
	public void tearDown() throws Exception {
		FxToolkit.hideStage();
		release(new KeyCode[] {});
		release(new MouseButton[] {});
	}

	@Test
	public void formContainsAllFields() {
		FxAssert.verifyThat("#barcodeLabel", LabeledMatchers.hasText("Barcode:"));
		FxAssert.verifyThat("#barcodeInput", NodeMatchers.isVisible());
		FxAssert.verifyThat("#nameLabel", LabeledMatchers.hasText("Name:"));
		FxAssert.verifyThat("#nameInput", NodeMatchers.isVisible());
		FxAssert.verifyThat("#unitLabel", LabeledMatchers.hasText("Unit:"));
		FxAssert.verifyThat("#unitChoice", NodeMatchers.isVisible());
		FxAssert.verifyThat("#sizeLabel", LabeledMatchers.hasText("Package size:"));
		FxAssert.verifyThat("#sizeInput", NodeMatchers.isVisible());
		FxAssert.verifyThat("#quantityLabel", LabeledMatchers.hasText("Quantity:"));
		FxAssert.verifyThat("#quantityInput", NodeMatchers.isVisible());
		FxAssert.verifyThat("#discard", LabeledMatchers.hasText("Discard"));
		FxAssert.verifyThat("#discard", NodeMatchers.isVisible());
		FxAssert.verifyThat("#confirm", LabeledMatchers.hasText("Confirm"));
		FxAssert.verifyThat("#confirm", NodeMatchers.isVisible());
	}
	
	@Test(expected = EmptyNodeQueryException.class)
	public void formDiscardsProperly() {
		new FxRobot().clickOn("#discard");
		FxAssert.verifyThat("#addItemScene", NodeMatchers.isNull());
	}

}
