package rs.ac.bg.etf.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;
import org.testfx.matcher.control.ComboBoxMatchers;
import org.testfx.matcher.control.LabeledMatchers;
import org.testfx.matcher.control.TableViewMatchers;
import org.testfx.matcher.control.TextInputControlMatchers;

import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DialogPane;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import rs.ac.bg.etf.controller.MainPageController;
import rs.ac.bg.etf.database.Database;
import rs.ac.bg.etf.database.Item;
import rs.ac.bg.etf.database.Unit;

public class StorageManagerAppTest extends ApplicationTest {

	private MainPageController controller;

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(StorageManagerApp.class.getResource("/MainPage.fxml"));
		Parent mainNode = loader.load();
		controller = loader.getController();
		controller.initializeTable();
		stage.setScene(new Scene(mainNode));
		stage.show();
		stage.toFront();
	}

	@After
	public void tearDown() throws Exception {
		FxToolkit.hideStage();
		release(new KeyCode[] {});
		release(new MouseButton[] {});
	}

	@Test
	public void shouldContainLabelWithText() {
		FxAssert.verifyThat("#tableHeadingLabel", NodeMatchers.isVisible());
		FxAssert.verifyThat("#tableHeadingLabel", LabeledMatchers.hasText("All available items in the storage:"));
	}

	@Test
	public void tableExists() {
		FxAssert.verifyThat("#table", NodeMatchers.isVisible());
	}

	@Test
	public void tableShouldBeEmpty() {
		FxAssert.verifyThat("#table", TableViewMatchers.hasNumRows(0));
	}

	@Test
	public void tableHasExactly5Columns() {
		TableView<Item> table = new FxRobot().lookup("#table").query();
		assertEquals(5, table.getColumns().size());
	}

	@Test
	public void tableColumnsHaveCorrectText() {
		String[] columnText = { "Barcode", "Name", "Size", "Unit", "Quantity" };
		TableView<Item> table = new FxRobot().lookup("#table").query();
		for (int i = 0; i < 5; i++) {
			assertEquals(columnText[i], table.getColumns().get(i).getText());
		}
	}

	@Test
	public void hasMenuWithOptions() {
		FxAssert.verifyThat("#mainMenu", NodeMatchers.isVisible());
		FxAssert.verifyThat("#fileMenu", LabeledMatchers.hasText("File"));
		MenuBar menuBar = new FxRobot().lookup("#mainMenu").query();
		assertEquals(menuBar.getMenus().get(0).getItems().get(0).getText(), "Quit");
	}

	@Test
	public void hasButtonsAboveHeader() {
		FxAssert.verifyThat("#addButton", NodeMatchers.isVisible());
		FxAssert.verifyThat("#addButton", LabeledMatchers.hasText("Add"));
		FxAssert.verifyThat("#removeButton", NodeMatchers.isVisible());
		FxAssert.verifyThat("#removeButton", LabeledMatchers.hasText("Remove"));
		FxAssert.verifyThat("#editButton", NodeMatchers.isVisible());
		FxAssert.verifyThat("#editButton", LabeledMatchers.hasText("Edit"));
		FxAssert.verifyThat("#exportButton", NodeMatchers.isVisible());
		FxAssert.verifyThat("#exportButton", LabeledMatchers.hasText("Export"));
	}

	@Test
	public void addsElementToTableCorrectly() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137962", 1);
		controller.addToTable(item);
		FxAssert.verifyThat("#table", TableViewMatchers.hasNumRows(1));
		Database.getInstance().removeItem(item);
	}

	@Test
	public void addButtonOpensForm() {
		new FxRobot().clickOn("#addButton");
		FxAssert.verifyThat("#addItemScene", NodeMatchers.isVisible());
	}

	@Test
	public void discardDoesntAffectData() {
		clickOn("#addButton");
		clickOn("#barcodeInput");
		write("658469585");
		clickOn("#nameInput");
		write("Laptop");
		clickOn("#sizeInput");
		write("1");
		clickOn("#quantityInput");
		write("1");
		new FxRobot().clickOn("#discard");
		assertEquals(0, Database.getInstance().getItems().size());
	}

	@Test
	public void submittingFormAddsItemToTable() {
		clickOn("#addButton");
		clickOn("#barcodeInput");
		write("658469585");
		clickOn("#nameInput");
		write("Laptop");
		clickOn("#sizeInput");
		write("1");
		clickOn("#quantityInput");
		write("1");
		clickOn("#confirm");
		assertEquals(1, Database.getInstance().getItems().size());
		Database.getInstance().removeItem(new Item("Laptop", Unit.PIECE, 1, "658469585", 1));
	}

	@Test
	public void ignoreFormSubmitIfFormIsNotFilled() {
		clickOn("#addButton");
		clickOn("#barcodeInput");
		write("658469585");
		new FxRobot().clickOn("#confirm");
		assertEquals(0, Database.getInstance().getItems().size());
		FxAssert.verifyThat("#addItemScene", NodeMatchers.isVisible());
	}

	@Test
	public void removeWithoutSelectionDoesNothing() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().addItem(item);
		TableView<Item> table = new FxRobot().lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		clickOn("#removeButton");
		FxAssert.verifyThat("#table", TableViewMatchers.hasNumRows(1));
		Database.getInstance().removeItem(item);
	}

	@Test
	public void removeDeletesSelectedItemFromTable() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().addItem(item);
		TableView<Item> table = new FxRobot().lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().select(0);
		clickOn("#removeButton");
		FxAssert.verifyThat("#table", TableViewMatchers.hasNumRows(0));
	}

	@Test
	public void removeDeletesMultipleSelectedItemsFromTable() {
		Item item1 = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Item item2 = new Item("Laptop", Unit.PIECE, 1.0, "978020137962", 1);
		Database.getInstance().addItem(item1);
		Database.getInstance().addItem(item2);
		TableView<Item> table = new FxRobot().lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().selectAll();
		clickOn("#removeButton");
		FxAssert.verifyThat("#table", TableViewMatchers.hasNumRows(0));
	}

	@Test
	public void editWithNoSelectionShowsAlert() {
		clickOn("#editButton");
		final List<Window> allWindows = new ArrayList<>(robotContext().getWindowFinder().listWindows());
		Collections.reverse(allWindows);

		Stage alert = (Stage) allWindows.stream().filter(window -> window instanceof javafx.stage.Stage)
				.filter(window -> ((javafx.stage.Stage) window).getModality() == Modality.APPLICATION_MODAL).findFirst()
				.orElse(null);
		assertNotNull(alert);

		final DialogPane dialogPane = (DialogPane) alert.getScene().getRoot();
		assertEquals("Message", dialogPane.getHeaderText());
		assertEquals("Please select exactly one item.", dialogPane.getContentText());
	}

	@Test
	public void clickOnEditOpensFormAndFillsDetails() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().addItem(item);
		TableView<Item> table = lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().select(0);
		clickOn("#editButton");
		FxAssert.verifyThat("#barcodeInput", TextInputControlMatchers.hasText("978020137961"));
		FxAssert.verifyThat("#nameInput", TextInputControlMatchers.hasText("Monitor"));
		FxAssert.verifyThat("#sizeInput", TextInputControlMatchers.hasText("1.0"));
		FxAssert.verifyThat("#quantityInput", TextInputControlMatchers.hasText("1"));
		FxAssert.verifyThat("#unitChoice", ComboBoxMatchers.hasSelectedItem("pc"));
		Database.getInstance().removeItem(item);
	}

	@Test
	public void discardButtonDoesntModifyEditedItem() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().addItem(item);
		TableView<Item> table = lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().select(0);
		clickOn("#editButton");
		clickOn("#nameInput");
		eraseText(1);
		clickOn("#discard");
		Item editedItem = table.getSelectionModel().getSelectedItem();
		assertEquals(editedItem.getBarcode(), item.getBarcode());
		assertEquals(editedItem.getName(), item.getName());
		assertEquals(editedItem.getQuantity(), item.getQuantity());
		assertEquals(editedItem.getSize(), item.getSize());
		assertEquals(editedItem.getUnit(), item.getUnit());
		Database.getInstance().removeItem(item);
	}
	
	@Test
	public void confirmButtonModifiesEditedItem() {
		Item item = new Item("Monitor", Unit.PIECE, 1.0, "978020137961", 1);
		Database.getInstance().addItem(item);
		TableView<Item> table = lookup("#table").query();
		table.setItems(FXCollections.observableArrayList(Database.getInstance().getItems()));
		table.getSelectionModel().select(0);
		clickOn("#editButton");
		clickOn("#nameInput");
		eraseText(7);
		write("Apple");
		clickOn("#sizeInput");
		eraseText(3);
		write("100.0");
		clickOn("#quantityInput");
		eraseText(1);
		write("5");
		clickOn("#unitChoice");
		type(KeyCode.UP);
		type(KeyCode.UP);
		type(KeyCode.ENTER);
		clickOn("#confirm");
		Item editedItem = table.getItems().get(0);
		assertEquals(item.getBarcode(), editedItem.getBarcode());
		assertEquals("Apple", editedItem.getName());
		assertEquals(new Integer(5), editedItem.getQuantity());
		assertEquals(new Double(100.0), editedItem.getSize());
		assertEquals(Unit.KILOGRAM, editedItem.getUnit());
		Database.getInstance().removeItem(editedItem);
	}
}
