package rs.ac.bg.etf.ui;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import rs.ac.bg.etf.controller.Util;
import rs.ac.bg.etf.database.Database;
import rs.ac.bg.etf.database.Item;
import rs.ac.bg.etf.database.Unit;

public class UtilTest {
	private String filePath;
	private Item item1 = new Item("Monitor", Unit.PIECE, 1.0, "596854645", 112);
	private Item item2 = new Item("Apple", Unit.KILOGRAM, 100.0, "125636598", 50);

	@Before
	public void setUp() {
		filePath = Paths.get(File.separator, "Users", "ivankulezic", "Desktop", "report.txt").toAbsolutePath()
				.toString();
		Database.getInstance().addItem(item1);
		Database.getInstance().addItem(item2);
		Util.exportData(filePath);
	}

	@After
	public void tearDown() {
		new File(filePath).delete();
		Database.getInstance().removeItem(item1);
		Database.getInstance().removeItem(item2);
	}

	@Test
	public void outputFileIsCreated() {
		File outFile = new File(filePath);
		assertEquals(true, outFile.exists());
		assertEquals(true, outFile.isFile());
	}

	@Test
	public void outputFileContainsCorrectData() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))) {
			assertEquals("Current items present in storage:", br.readLine());
			for (int i = 0; i < 2; i++) {
				assertEquals("", br.readLine());
			}
			for (Item item : Database.getInstance().getItems()) {
				assertEquals(item.toString(), br.readLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
